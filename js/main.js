// Initializing tabs
for (let de of document.querySelector('.tabs').children) {
    de.addEventListener("click", hndTabSelect);
    if (de.classList.contains("active"))
        document.getElementById(de.dataset.tabid).style.display = "block";
    else
        document.getElementById(de.dataset.tabid).style.display = "none";
}


function hndTabSelect(ev) {
    let deActiveTab = document.querySelector('.tabtitle.active');
    deActiveTab.classList.remove("active");
    document.getElementById(deActiveTab.dataset.tabid).style.display = "none";

    ev.currentTarget.classList.add("active");
    document.getElementById(ev.currentTarget.dataset.tabid).style.display = "block";
}